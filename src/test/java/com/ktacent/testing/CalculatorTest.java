package com.ktacent.testing;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {
    private Calculator calculatorUnderTest;

    private static Instant startedAt;

    @BeforeAll
    public static void initStartingTime() {
        System.out.println("Appel avant tous les test");
        startedAt = Instant.now();
    }

    @AfterAll
    public static void showTestDuration() {
        System.out.println("Appel après tous les tests");
        final Instant endedAt = Instant.now();
        final long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.println(MessageFormat.format("Durée de tests : {0} ms", duration));
    }

    @BeforeEach
    public void initCalculator() {
        calculatorUnderTest = new Calculator();
        System.out.println("Appel avant chaque test");
    }

    @AfterEach
    public void undefCalculator() {
        System.out.println("Appel après chaque test");
        calculatorUnderTest = null;
    }

    @Test
    void testAddTwoPositiveNumbers() {
        // ARRANGE
        int a = 2;
        int b = 3;

        // ACT
        int somme = calculatorUnderTest.add(a, b);

        // ASSERT
        assertEquals(5, somme);
    }

    @Test
    void multiply_shouldReturnTheProduct_ofTwoInteger() {
        // ARRANGE
        int a = 2;
        int b = 3;

        // ACT
        int produit = calculatorUnderTest.multiply(a, b);

        // ASSERT
        assertEquals(6, produit);
    }

    @ParameterizedTest(name = "{0} x 0 doit être égale à 0")
    @ValueSource(ints = {1, 2, 4, 5, 100, 3049, 40093, 503049})
    public void multiply_shouldReturnZero_ofZeroWithMultipleInteger(int arg) {
        // ARRANGE - Tout est prêt

        // ACT -- Multiplier par zéro
        final  int actualResult = calculatorUnderTest.multiply(arg, 0);

        // ASSERT -- Ca vaut toujours zéro
        assertEquals(0, actualResult);
    }

    @ParameterizedTest(name = "{0} + {1} doit être égal à {2}")
    @CsvSource({"1, 2, 3", "2, 3, 5", "10, 34, 44", "42, 57, 99"})
    public void add_shouldReturnTheSum_ofMultipleIntegers(int arg1, int arg2, int expectedResult) {
        // ARRANGE - Tout est prêt

        // ACT
        int actualResult = calculatorUnderTest.add(arg1, arg2);

        // ASSERT
        assertEquals(actualResult, expectedResult);
    }

    @Test
    @Timeout(1)
    public void longCalCul_shouldComputeInLessThantSecond() {
        // ARRANGE - Rien à faire

        // ACT
        calculatorUnderTest.longCalculation();

        // ASSERT
        //...
    }
}
